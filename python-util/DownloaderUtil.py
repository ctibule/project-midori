import requests
import xml.etree.ElementTree as ET
from qbittorrent import Client

"""
DownloaderUtil - contains functions that will be used to assist the operation of the application, mostly concerning the
download of XML files and the file names.

Author: Carl Tibule
Date: 3 July 2017
Project Name: project-midori
"""


def save_download_xml_locally(file_name, link):
    """
    Saves the XML file which contains episodes available to download for the day to a local location.
    :param file_name: name of the XML file that will be created
    :param link: link to where the XML file will be downloaded from (usually HorribleSubs)
    :param dl_path: location on where the XML file will be downloaded to locally
    :return: N/A
    """

    xml_file = open(file_name + '.xml', 'w')
    xml_link = requests.get(link)
    xml_file.write(xml_link.text)


def retrieve_xml(link):
    """
    Returns XML information from a website (usually HorribleSubs) as string
    :param link: link to where the XML file will be downloaded from
    :return: XML as string
    """

    return requests.get(link).text


def get_episode_number(file_name):
    """
    Gets the episode number from the file name
    :param file_name:
    :return: episode number
    """
    return file_name.replace('[HorribleSubs]', '', 1).replace('.mkv', '', 1).split(' [')[0].strip()


def get_resolution(file_name):
    """
    Gets the resolution from the file name
    :param file_name:
    :return: resolution
    """
    return file_name.replace('[HorribleSubs]', '').replace('.mkv', '').split('[')[1].replace(']', '').strip()


def download_anime(xml):
    """
    Downloads anime from HorrribleSubs according to the choices made by the user
    :param xml: XML as string that contains download information
    :return: N/A
    """

    xml_root = ET.fromstring(xml)
    qb_client = Client('http://127.0.0.1:8080/')
    qb_client.login('User1', 'User1User1')
