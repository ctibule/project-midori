package io.ctibule.midori.model;

import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Channel - represents a group of items available for download at one particular time
 *
 * Author: Carl Tibule
 * Date: 2 July 2017
 * Project: project-midori
 */
public class Channel {
    private StringProperty channelTitle;
    private StringProperty description;
    private StringProperty channelLink;
    private Date date;
    private final SimpleDateFormat sdf = new SimpleDateFormat("d-MMMM-yyyy-H:m:s");
    private ArrayList<Episode> episodes;

    /**
     * Constructs an instance of Channel object that accepts 3 arguments
     *
     * @param channelTitle - the title of the channel
     * @param description  - the description of the channel
     * @param channelLink  - the link of the channel's origin
     * @param episodes - list of episodes associated with this channel
     */
    public Channel(String channelTitle, String description, String channelLink, ArrayList<Episode> episodes) {
        this.channelTitle = new SimpleStringProperty(channelTitle);
        this.description = new SimpleStringProperty(description);
        this.channelLink = new SimpleStringProperty(channelLink);
        this.episodes = new ArrayList<>();

        //Get current datetime
        DateFormat dateFormat = sdf;
        date = new Date();
        dateFormat.format(date);
    }

    /**
     * Constructs an instance of Channel object that does not accept any arguments
     */
    public Channel() {
        this("", "", "", null);
    }

    /**
     *
     * @return channel title as property
     */
    public StringProperty getChannelTitleProperty()
    {
        return this.channelTitle;
    }

    /**
     * @return value the title of the channel
     */
    public String getChannelTitle() {
        return this.channelTitle.get();
    }

    /**
     * Sets the title of the channel
     *
     * @param channelTitle the string that will be used as the channel title
     */
    public void setChannelTitle(String channelTitle) {
        this.channelTitle.set(channelTitle);
    }

    /**
     *
     * @return channel's description as property
     */
    public StringProperty getDescriptionProperty()
    {
        return this.description;
    }

    /**
     *
     * @return description of the channel
     */
    public String getDescription() {
        return this.description.get();
    }

    /**
     * Sets the description of the channel
     * @param description the string that will be used as the description of the channel
     */
    public void setDescription(String description)
    {
        this.description.set(description);
    }

    /**
     *
     * @return channel link as property
     */
    public StringProperty getChannelLinkProperty()
    {
        return this.channelLink;
    }

    /**
     *
     * @return link of channel's origin
     */
    public String getChannelLink()
    {
        return this.channelLink.get();
    }

    /**
     * Sets the link of the channel
     * @param channelLink the string that will be used as the link of the channel
     */
    public void setChannelLink(String channelLink)
    {
        this.channelLink.set(channelLink);
    }

    /**
     *
     * @return string representation of the channel's date
     */
    public String getDate()
    {
        return this.date.toString();
    }

    /**
     *
     * @return list of episodes included in this channel
     */
    public ArrayList<Episode> getEpisodes()
    {
        return this.episodes;
    }

    /**
     * Attaches a list of episodes to this channel
     * @param episodes list of episodes
     */
    public void setEpisodes(ArrayList<Episode> episodes)
    {
        this.episodes = episodes;
    }
}
