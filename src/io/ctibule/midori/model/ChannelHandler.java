package io.ctibule.midori.model;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import java.util.ArrayList;
/**
 * ChannelHandler - dictates how the XML file should be read and converted into objects.
 *
 * Author: Carl Tibule
 * Date: 3 July 2017
 * Project: project-midori
 */
public class ChannelHandler extends DefaultHandler
{
    private Channel channel;
    private StringBuilder stringBuilder;
    private ArrayList<Episode> episodes;
    private Episode episode;

    /**
     * Constructs an instance of ChannelHandler using the constructor of its parent class
     */
    public ChannelHandler()
    {
        super();
    }

    /**
     * Defines actions taken when a start element is found
     * @param uri
     * @param localName
     * @param qName
     * @param attributes
     * @throws SAXException
     */
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
    {
        //Instantiate Channel and StringBuilder object when "channel" start element is found
        if (qName.equalsIgnoreCase("CHANNEL"))
        {
            channel = new Channel();
            stringBuilder = new StringBuilder();
        }
        //Instantiate episode object when "item" start element is found
        else if (qName.equalsIgnoreCase("ITEM"))
        {
            episode = new Episode();
        }
    }

    /**
     * Defines actions taken when an end element is found
     * @param uri
     * @param localName
     * @param qName
     * @throws SAXException
     */
    public void endElement(String uri, String localName, String qName) throws SAXException
    {
        //Add list of episodes to channel object when "channel" end element is found
        if (qName.equalsIgnoreCase("CHANNEL"))
        {
            channel.setEpisodes(episodes);
        }
        else if (qName.equalsIgnoreCase("LINK"))
        {
            //When link end element is found and episode has not yet been instantiated, set the link to channel object.
            if(episode == null)
            {
                channel.setChannelLink(stringBuilder.toString());
            }
            //Otherwise, set the link as episode's magnet link
            else
            {
                episode.setMagnetLink(stringBuilder.toString());
            }
        }

        if (qName.equalsIgnoreCase("TITLE"))
        {
            //When link end element is found and episode has not yet been instantiated, set the title to channel object
            if (episode == null)
            {
                channel.setChannelTitle(stringBuilder.toString());
                episodes = new ArrayList<>();
            }
            else if (episode != null)
            {
                episode.setFileName(stringBuilder.toString());
            }
        }
        //When "item" end tag is found, add the episode to the list of episodes and set the episode object to null
        else if (qName.equalsIgnoreCase("ITEM"))
        {
            if (episode != null)
            {
                episodes.add(episode);
                episode = null;
            }
        }
        //When "guid" end tag is found, set it to episode's guid attribute
        else if (qName.equalsIgnoreCase("GUID"))
        {
            episode.setGuid(stringBuilder.toString());
        }
        //When "pubDate" end tag is found, set it to episode's pubDate attribute
        else if (qName.equalsIgnoreCase("PUBDATE"))
        {
            episode.setPubDate(stringBuilder.toString());
        }

        //Reset stringBuilder after text between xml tags have been red
        stringBuilder.setLength(0);
    }

    /**
     * Defines actions taken when characters between XML tags are found
     * @param ch
     * @param start
     * @param length
     * @throws SAXException
     */
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException
    {
        //Read characters between xml tags and add it to stringBuilder
        for(int i = start; i < start + length; i++)
        {
            stringBuilder.append(ch[i]);
        }
    }

    /**
     *
     * @return channel of this handler
     */
    public Channel getChannel()
    {
        return this.channel;
    }
}
