package io.ctibule.midori.model;

import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import org.python.util.PythonInterpreter;

/**
 * Episode - represents a single item in a channel in HorribleSubs XML, and a single episode in an anime series.
 *
 * Author: Carl Tibule
 * Date: 3 July 2017
 * Project: project-midori
 */

public class Episode {
    private StringProperty fileName;
    private StringProperty magnetLink;
    private StringProperty guid;
    private Date pubDate;
    private StringProperty episodeNumber;
    private StringProperty resolution;
    private final SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy H:m:s +0000");
    private BooleanProperty toDownload;
    private StringProperty downloadPath;

    /**
     * Constructs an instance of Episode object that accepts 4 arguments
     * @param fileName name of the file that is associated with this object
     * @param magnetLink magnet link of the file
     * @param guid
     * @param pubDate date when the file was published
     */
    public Episode(String fileName, String magnetLink, String guid, String pubDate)
    {
        this.fileName = new SimpleStringProperty(fileName);
        this.magnetLink = new SimpleStringProperty(magnetLink);
        this.guid = new SimpleStringProperty(guid);
        this.toDownload = new SimpleBooleanProperty(false);
        this.downloadPath = new SimpleStringProperty(String.format("%s%sDownloads%s", System.getProperty("user.home"),
                File.separator, File.separator));

        //Convert the pubDate to a Date object
        DateFormat dateFormat = sdf;
        this.pubDate = new Date();
        dateFormat.format(this.pubDate);
    }

    /**
     * Constructs an instance of Episode object that accepts no arguments
     */
    public Episode()
    {
        this("", "", "", "");
    }

    /**
     *
     * @return file name as property
     */
    public StringProperty getFileNameProperty()
    {
        return this.fileName;
    }

    /**
     *
     * @return file name
     */
    public String getFileName()
    {
        return this.fileName.get();
    }

    /**
     * Sets the file name of this episode
     * @param fileName the String that will be used as the file name
     */
    public void setFileName(String fileName)
    {
        this.fileName.set(fileName);

        //Get episode number and resolution
        PythonInterpreter interpreter = new PythonInterpreter();
        interpreter.execfile("python-util/DownloaderUtil.py");
        String epNumFromPy = interpreter.eval(String.format("get_episode_number(\"%s\")", fileName)).asString();
        String resFromPy = interpreter.eval(String.format("get_resolution(\"%s\")", fileName)).asString();

        this.episodeNumber = new SimpleStringProperty(epNumFromPy);
        this.resolution = new SimpleStringProperty(resFromPy);
    }

    /**
     *
     * @return magnet link as property
     */
    public StringProperty getMagnetLinkProperty()
    {
        return this.magnetLink;
    }

    /**
     *
     * @return magnet link associated with this episode
     */
    public String getMagnetLink()
    {
        return this.magnetLink.get();
    }

    /**
     * Sets the magnet link of this episode
     * @param magnetLink string that will be used as the magnet link of this episode
     */
    public void setMagnetLink(String magnetLink)
    {
        String fixedMagnetLink = magnetLink.replace("&", "&amp;");
        this.magnetLink.set(fixedMagnetLink);
    }

    /**
     *
     * @return guid as property
     */
    public StringProperty getGuidProperty()
    {
        return this.guid;
    }

    /**
     *
     * @return GUID of this episode
     */
    public String getGuid()
    {
        return this.guid.get();
    }

    /**
     * Sets the GUID of this episode
     * @param guid string that will be used as the GUID of this episode
     */
    public void setGuid(String guid)
    {
        this.guid.set(guid);
    }

    /**
     *
     * @return date the episode was published
     */
    public Date getPubDate()
    {
        return this.pubDate;
    }

    /**
     * Sets the pubDate of this episode object
     * @param date date as string
     */
    public void setPubDate(String date)
    {
        try
        {
            this.pubDate = sdf.parse(date);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
    }

    /**
     *
     * @return episode number as property
     */
    public StringProperty getEpisodeNumberProperty()
    {
        return this.episodeNumber;
    }
    /**
     *
     * @return episode number
     */
    public String getEpisodeNumber()
    {
        return this.episodeNumber.get();
    }

    /**
     * Sets the episode number
     * @param episodeNumber new episode number
     */
    public void setEpisodeNumber(String episodeNumber)
    {
        this.episodeNumber.set(episodeNumber);
    }

    /**
     *
     * @return resolution as property
     */
    public StringProperty getResolutionProperty()
    {
        return this.resolution;
    }

    /**
     *
     * @return resolution
     */
    public String getResolution()
    {
        return this.resolution.get();
    }

    /**
     * Sets the resolution of this Episode
     * @param resolution the resolution of the object
     */
    public void setResolution(String resolution)
    {
        this.resolution.set(resolution);
    }

    /**
     *
     * @return variable that identifies whether this episode will be downloaded as property
     */
    public BooleanProperty getToDownloadProperty()
    {
        return this.toDownload;
    }

    /**
     *
     * @return value of whether this episode will be downloaded or not
     */
    public boolean getToDownload()
    {
        return this.toDownload.get();
    }

    /**
     * Sets the state of whether this episode will be downloaded or not
     * @param toDownload true if the episode will be downloaded, false if otherwise
     */
    public void setToDownload(boolean toDownload)
    {
        this.toDownload.set(toDownload);
    }

    /**
     *
     * @return download path as property
     */
    public StringProperty getDownloadPathProperty()
    {
        return this.downloadPath;
    }

    /**
     *
     * @return download path of this Episode
     */
    public String getDownloadPath()
    {
        return this.downloadPath.get();
    }

    /**
     * Changes download path of this episode
     * @param downloadPath where the episode will be downloaded
     */
    public void setDownloadPath(String downloadPath)
    {
        this.downloadPath.set(downloadPath);
    }
}
