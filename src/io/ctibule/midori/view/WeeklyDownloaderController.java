package io.ctibule.midori.view;

import io.ctibule.midori.AppController;
import io.ctibule.midori.model.*;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.input.MouseEvent;
import javafx.stage.DirectoryChooser;

import org.python.util.PythonInterpreter;
import org.xml.sax.InputSource;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import java.io.File;
import java.io.StringReader;
import java.util.regex.Pattern;

/**
 * io.ctibule.midori.view.WeeklyDownloaderController - provides functionality to the Weekly Downloader scene
 *
 * Author: Carl Tibule
 * Date: 29 June 2017
 * Project Name: project-midori
 */
public class WeeklyDownloaderController{

    @FXML
    private TableView<Episode> weeklyDownloadTable;
    @FXML
    private TableColumn<Episode, Boolean> includeInDownload;
    @FXML
    private TableColumn<Episode, String> titleColumn;
    @FXML
    private TableColumn<Episode, String> resolution;
    @FXML
    private TableColumn<Episode, String> downloadPath;

    private ObservableList<Episode> episodesList;
    private final PythonInterpreter interpreter = new PythonInterpreter();
    @FXML
    private void initialize()
    {
        try
        {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            ChannelHandler handler = new ChannelHandler();

            //Get XML file from HorribleSubs and store it to a String variable
            this.interpreter.execfile("python-util/DownloaderUtil.py");
            String xmlOutput = this.interpreter.eval("retrieve_xml('http://horriblesubs.info/rss.php?res=all')").asString();

            //Parse XML stored as String variable
            saxParser.parse(new InputSource(new StringReader(xmlOutput)), handler);

            initGui(handler);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Sets the items of the table and displays important information to the column
     */
    public void initGui(ChannelHandler handler)
    {
        //Cast ArrayList to ObservableList
        this.episodesList = FXCollections.observableArrayList(handler.getChannel().getEpisodes());

        //Allow TableView to be editable
        weeklyDownloadTable.setEditable(true);
        weeklyDownloadTable.setItems(episodesList);

        //Set the cells of includeInDownload column to contain CheckBoxTableCell
        includeInDownload.setCellFactory(CheckBoxTableCell.forTableColumn(includeInDownload));
        includeInDownload.setCellValueFactory(cellData -> cellData.getValue().getToDownloadProperty());

        //CustomTableCellFactory
        CustomTableCellFactory cellFactoryNoArgs = new CustomTableCellFactory();

        //Title Column
        titleColumn.setCellValueFactory(cellData -> cellData.getValue().getEpisodeNumberProperty());
        titleColumn.setCellFactory(column -> {
            //Re-implement how TableCell is displayed
            return new TableCell<Episode, String>(){
                @Override
                protected void updateItem(String item, boolean empty)
                {
                    super.updateItem(item, empty);
                    super.setAlignment(Pos.CENTER);

                    if(item == null || empty)
                    {
                        setText("Missing Episode Number!");
                    }
                    else
                    {
                        setText(item);
                    }
                }
            };
        });

        //Resolution Column
        resolution.setCellValueFactory(cellData -> cellData.getValue().getResolutionProperty());
        resolution.setCellFactory(column ->{
            //Re-implemenets how TableCell is displayed
            return new TableCell<Episode, String>(){
                @Override
                protected void updateItem(String item, boolean empty)
                {
                    super.updateItem(item, empty);
                    super.setAlignment(Pos.CENTER);

                    if(item == null || empty)
                    {
                        setText("Missing Resolution!");
                    }
                    else
                    {
                        setText(item);
                    }
                }
            };
        });

        //Add EventHandler that looks for double-click
        EventHandler eventHandler = new EventHandler<MouseEvent>(){
            public void handle(MouseEvent event)
            {
                if(event.getClickCount() == 2)
                {
                    try
                    {
                        //Get the highlighted object from the table
                        Episode episode = weeklyDownloadTable.getSelectionModel().getSelectedItem();

                        //Set the initial directory to the object's default download path and show it to the user
                        DirectoryChooser directoryChooser = new DirectoryChooser();
                        directoryChooser.setTitle("Change Download Path");
                        directoryChooser.setInitialDirectory(new File(episode.getDownloadPath()));
                        File newDownloadPath = directoryChooser.showDialog(weeklyDownloadTable.getScene().getWindow());

                        //Set the new download path if something has been selected by the user.
                        if(newDownloadPath != null)
                        {
                            episode.setDownloadPath(newDownloadPath.getAbsolutePath());
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        };

        //DownloadPath column
        downloadPath.setCellValueFactory(cellData -> cellData.getValue().getDownloadPathProperty());
        downloadPath.setCellFactory(column -> {
            //Re-implements how TableCell is displayed and add mouse event to it.
            return new TableCell<Episode, String>(){
                @Override
                protected void updateItem(String item, boolean empty)
                {
                    super.updateItem(item, empty);

                    if(item == null || empty)
                    {
                        setText("Missing Resolution!");
                    }
                    else
                    {
                        setText(item);
                    }

                    super.setAlignment(Pos.CENTER);
                    super.setOnMouseClicked(eventHandler);
                }
            };
        });
    }

    /**
     * Filters out episodes in episodes list that are set to be downloaded upon clicking the Download button
     */
    @FXML
    public void btnDownload_Click()
    {
        //Build XML file
        StringBuilder xml = new StringBuilder();
        xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><MidoriManualDownload>");

        for(Episode episode : this.episodesList)
        {
            if(episode.getToDownload())
            {
                xml.append(String.format("<Anime DownloadPath=\"%s\" MagnetLink=\"%s\"/>", episode.getDownloadPath(),
                        episode.getMagnetLink()));
            }
        }

        xml.append("</MidoriManualDownload>");

        interpreter.eval(String.format("download_anime('%s')", xml.toString()));
    }
}
