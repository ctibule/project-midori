package io.ctibule.midori.view;

import io.ctibule.midori.AppController;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Button;
import java.util.Optional;

/**
 * io.ctibule.midori.view.RootLayoutController - provides functionality to the menu items in the BorderPane
 *
 * Author: Carl Tibule
 * Date: 29 June 2017
 * Project Name: project-midori
 */
public class RootLayoutController {
    private AppController appController;

    public void setMainApp(AppController appController)
    {
        this.appController = appController;
    }

    /**
     * mnuCloseClick - handles the click event of Close menu item. An alert type will be shown confirming whether the
     * user intends to close the application or not.
     */
    @FXML
    private void mnuCloseClick()
    {
        Alert alert = new Alert(AlertType.CONFIRMATION,
                "Are you sure you want to close this application?",
                ButtonType.YES, ButtonType.NO);
        alert.setTitle("Closing the App");

        //Change default button to "No"
        Button btnYes = (Button)alert.getDialogPane().lookupButton(ButtonType.YES);
        btnYes.setDefaultButton(false);

        Button btnNo = (Button)alert.getDialogPane().lookupButton(ButtonType.NO);
        btnNo.setDefaultButton(true);

        //Close the app if the user clicks "Yes".
        Optional<ButtonType> result = alert.showAndWait();

        if(result.isPresent() && result.get() == ButtonType.YES)
        {
            System.exit(0);
        }
    }

    /**
     * mnuWeeklyDownloader - handles the click event of the Weekly Downloader menu item. The Weekly Downloader scene
     * will be set to the center of the stage.
     */
    @FXML
    private void mnuWeeklyDownloader()
    {
        appController.setCenter("view\\WeeklyDownloader.fxml");

    }
}
