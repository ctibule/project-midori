package io.ctibule.midori.view;

import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import javafx.geometry.Pos;

/**
 * CustomTableCellFactory - a customized table cell factory that center-aligns text and adds Mouse Click event and/or
 * context menu if present
 *
 * Author: Carl Tibule
 * Date 8 July 2017
 * Project: project-midori
 */
public class CustomTableCellFactory implements Callback<TableColumn, TableCell>
{
    private EventHandler clickEvent;
    private ContextMenu contextMenu;

    /**
     * Constructs a CustomTableCellFactory with two arguments
     * @param clickEvent
     * @param contextMenu
     */
    public CustomTableCellFactory(EventHandler clickEvent, ContextMenu contextMenu)
    {
        this.clickEvent = clickEvent;
        this.contextMenu = contextMenu;
    }

    /**
     * Constructs a CustomTableCellFactory with clickEvent as the sole argument
     * @param clickEvent
     */
    public CustomTableCellFactory(EventHandler clickEvent)
    {
        this(clickEvent, null);
    }

    /**
     * Constructs a CustomTableCellFactory with contextMenu as the sole argument
     * @param contextMenu
     */
    public CustomTableCellFactory(ContextMenu contextMenu)
    {
        this(null, contextMenu);
    }

    /**
     * Constructs a CustomTableCellFactory with no arguments
     */
    public CustomTableCellFactory()
    {
        this(null, null);
    }

    public TableCell call(TableColumn p)
    {
        TableCell cell = new TableCell()
        {
            @Override
            protected void updateItem(Object item, boolean empty)
            {
                super.updateItem(item, empty);
                super.setAlignment(Pos.CENTER);

                if(item != null)
                {
                    setText(item.toString());
                }
            }
        };

        //Right click context menu
        if(this.contextMenu != null)
        {
            cell.setContextMenu(this.contextMenu);
        }

        //Click Event
        if(this.clickEvent != null)
        {
            cell.setOnMouseClicked(this.clickEvent);
        }

        return cell;
    }
}
