package io.ctibule.midori;

import io.ctibule.midori.view.RootLayoutController;
import io.ctibule.midori.view.WeeklyDownloaderController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.Modality;

import java.io.IOException;

/**
 * io.ctibule.midori.AppController - This class provides functionality to the menu bar.
 *
 * Author: Carl Tibule
 * Date: 28 June 2017
 * Project Name: project-midori
 */
public class AppController extends Application{
    private Stage primaryStage;
    private BorderPane rootLayout;

    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage)
    {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Project Midori");

        //Initialize RootLayout
        initRootLayout();
        //setCenter();
    }

    public void initRootLayout()
    {
        try
        {
            //Load root layout from RootLayout.fxml
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(AppController.class.getResource("view/RootLayout.fxml"));
            this.rootLayout = (BorderPane)loader.load();

            //Show the scene containing the root layout
            Scene scene = new Scene(this.rootLayout);
            primaryStage.setScene(scene);
            primaryStage.setMaximized(true);
            primaryStage.show();

            //Give RootLayoutController access to this app
            RootLayoutController controller = loader.getController();
            controller.setMainApp(this);
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    public void setCenter(String path)
    {
        try
        {
            //Load FXML File for Weekly Downloader stage
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(AppController.class.getResource(path));
            AnchorPane center = (AnchorPane)loader.load();
            this.rootLayout.setCenter(center);
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     *
     * @return The primary stage of this application
     */
    public Stage getPrimaryStage()
    {
        return this.primaryStage;
    }
}
